image: node:10-alpine

stages:
    - build
    - test
    - release
    - deploy

variables:
    PUPPETEER_SKIP_CHROMIUM_DOWNLOAD: "true"
    CHROME_BIN: "/usr/bin/chromium-browser"

cache:
  paths:
    - node_modules/
    - dist/

.build-template: &build-template
  stage: build
  before_script:
    - npm install
  script:
    - npm run ng -- build --base-href .
  after_script:
    - version=$(awk 'BEGIN { FS=":"; RS="," } $1 ~ /"version"/ { gsub(/( |")/, "", $0); print $2 }' package.json)
    - sed -i '/^<head>$/ a\    <meta name="application-name" content="hokiegeek.net_tea" data-version="'${version}${version_suffix:+":${version_suffix}"}'" />' dist/index.html
    - head dist/index.html

.build-snapshot:
  <<: *build-template
  variables:
    version_suffix: "SNAPSHOT"
  only:
    - master
  except:
    - tags

build-branch:
  <<: *build-template
  variables:
    version_suffix: $CI_COMMIT_REF_NAME
  only:
    - branches
  except:
    - master

build-release:
  <<: *build-template
  script:
    - npm run build # Run optimized build
  only:
    - tags

## Linting
.lint-template: &lint-template
  stage: build
  script:
    - npm install
    - npm run lint
  retry: 1

lint:
  <<: *lint-template
  only:
    - tags

lint-warn:
  <<: *lint-template
  except:
    - master
    - tags
  allow_failure: true

## Unit testing
.node-unit-test: &node-unit-test
  stage: test
  before_script:
    - apk update && apk upgrade
    - echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories
    - echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories
    - apk add --no-cache chromium@edge nss@edge
  script:
    - npm run test
  only:
    - branches
    - tags
  except:
    - master
  retry: 2
      # coverage: /TODO/

unit-node:10:
  <<: *node-unit-test

unit-node:latest:
  <<: *node-unit-test
  image: node:alpine
  allow_failure: true

## Lifecycle Evaluation
.lifecycle-evaluation: &lifecycle-evaluation
  stage: test
  image: registry.gitlab.com/sonatype-nexus-community/nexus-lifecycle-gitlab:latest
  script:
    - evaluate
  after_script:
    - gitlab --no-issues
  retry: 1
  allow_failure: true

.evaluate-build:
  <<: *lifecycle-evaluation
  variables:
    IQ_STAGE: build
  only:
    - branches
  except:
    - master

.evaluate-stage:
  <<: *lifecycle-evaluation
  variables:
    IQ_STAGE: stage-release
  only:
    - master

.evaluate-release:
  <<: *lifecycle-evaluation
  variables:
    IQ_STAGE: release
  only:
    - tags

## Create container
.build-docker-image: &build-docker-image
  stage: release
  image: docker:git
  services:
    - docker:dind
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - head dist/index.html
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  retry: 1

create-container-dev:
  <<: *build-docker-image
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:SNAPSHOT
  except:
    - tags
    - master

create-container:
  <<: *build-docker-image
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    IMAGE_LATEST_TAG: $CI_REGISTRY_IMAGE:latest
  after_script:
    - docker tag $IMAGE_TAG $IMAGE_LATEST_TAG
    - docker push $IMAGE_LATEST_TAG
  only:
    - tags
  except:
    - branches

.server-connector-template: &server-connector-template
  image: alpine:latest
  before_script:
    - apk add --update --no-cache openssh
    - echo $HOST_SSH_KEY | base64 -d > /tmp/sshkey
      # - echo $HOST_SSH_KEY | sed -e 's/ /\n/g' | sed -r -e '1,4{:a;N;4!ba;s/\n/ /g}' | sed -r -e '/^-{5}END/,${:z;N;$!bz;s/\n/ /g}' > /tmp/sshkey
    - chmod 0600 /tmp/sshkey

add-proxy-server:
  <<: *server-connector-template
  stage: deploy
  script:
    - scp -i /tmp/sshkey -o StrictHostKeyChecking=no ./prod-conf/proxy-tea.conf $ADMIN_USER@${DEPLOY_HOST}:/tmp
    - ssh -i /tmp/sshkey -o StrictHostKeyChecking=no $ADMIN_USER@${DEPLOY_HOST} "sudo mkdir -p /etc/nginx/conf.d; sudo mv /tmp/proxy-tea.conf /etc/nginx/conf.d && sudo systemctl restart hgproxy.service"
      # - ssh -i /tmp/sshkey -o StrictHostKeyChecking=no $ADMIN_USER@${DEPLOY_HOST} "sudo systemctl restart hgproxy.service"
  only:
    - tags
  except:
    - branches

## Service restarter
.service-restarter: &service-restarter
  <<: *server-connector-template
  stage: deploy
  script:
    - scp -i /tmp/sshkey -o StrictHostKeyChecking=no ./prod-conf/${SERVICE} $ADMIN_USER@${DEPLOY_HOST}:/tmp
    - ssh -i /tmp/sshkey -o StrictHostKeyChecking=no $ADMIN_USER@${DEPLOY_HOST} "sudo mv /tmp/${SERVICE} /etc/systemd/system/ && sudo systemctl daemon-reload"
    - ssh -i /tmp/sshkey -o StrictHostKeyChecking=no $ADMIN_USER@${DEPLOY_HOST} "sudo systemctl enable ${SERVICE}; sudo systemctl restart ${SERVICE}"
  retry: 1

restart-service:
  <<: *service-restarter
  variables:
    SERVICE: hgtea.service
  environment:
    name: production
    url: http://tea.hokiegeek.net
  only:
    - tags
  except:
    - branches
